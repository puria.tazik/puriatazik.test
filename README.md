
# Description

I didn't use any grid system for this test and the task got implemented with two ways

  
# Prerequisites 
before going to installation guide you need to install nodejs and if you prefer working with yarn, install it 

# Installation Guide

* first install the dependencies:
### `yarn install`
Or
### `npm install`

* then run the project by running one of these commands:
### `yarn start`
Or
### `npm run start`  
  
