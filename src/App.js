import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/home";
import UnderConstruction from "./pages/underConstruction";

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/underconstruction">
            <UnderConstruction />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
