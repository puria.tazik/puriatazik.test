import React, { Component } from "react";
import UnderConstructionIMG from "../../assets/images/under-construction.png";

class UnderConstruction extends Component {
  render() {
    return (
      <div>
        <img src={UnderConstructionIMG} />
      </div>
    );
  }
}

export default UnderConstruction;
