import PropTypes from "prop-types";
import React from "react";
import styles from "./button.module.scss";

const Button = ({ onClick, children }) => {
  return (
    <div className={styles.button} onClick={onClick}>
      {children}
    </div>
  );
};

Button.propTypes = {
  onClick: PropTypes.func,
  children: PropTypes.node,
};

export default Button;
