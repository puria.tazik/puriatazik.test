import React, { useRef } from "react";
import PropTypes from "prop-types";
import styles from "./popupWithJsAway.module.scss";
import SharedContent from "../sharedContent";

const PopupWithJsAway = ({ isOpen, closeFn }) => {
  const contentJSRef = useRef();

  return isOpen ? (
    <div
      className={styles.container}
      onClick={(event) => {
        if (event.target !== contentJSRef.current) {
          return;
        }
        closeFn();
      }}
    >
      {/* click away area */}
      <div className={styles.clickAwayArea} onClick={closeFn} />
      {/* popup content */}
      <SharedContent ref={contentJSRef} closeFn={closeFn} />
    </div>
  ) : (
    <></>
  );
};

PopupWithJsAway.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  closeFn: PropTypes.func.isRequired,
};

export default PopupWithJsAway;
