import React from "react";
import styles from "./sharedContent.module.scss";
import { Link } from "react-router-dom";
import CloseSVG from "../../../../../assets/images/close.svg";
import VideoEmbeddedImg from "../../../../../assets/images/pricing-video-embedded.png";
import ProfileLinkImg from "../../../../../assets/images/pricing-profile-link.png";
import Button from "../../button/button";
import PropTypes from "prop-types";

const PricingItem = (props) => {
  return (
    <div className={styles.pricing_item}>
      <img src={props.item.image} className={styles.pi_image} />
      <div className={styles.pi_text}>{props.item.text}</div>
      <Button>{props.item.buttonText}</Button>
    </div>
  );
};

const SharedContent = React.forwardRef(({ closeFn }, ref) => {
  
  return (
    <div className={styles.content_holder} ref={ref}>
      <div className={styles.content}>
        <img className={styles.close} src={CloseSVG} onClick={closeFn} />
        <div className={styles.title}>
          <div className={styles.premium}>Premium</div>
          You Discovered a Premium Video!
        </div>
        <div className={styles.description}>
          Upgrade to <strong>Pro</strong> to share using profile link, OR <br />
          Upgrade to <strong>Premium</strong> to download &amp; embed actual
          video in social media feeds
          <Link className={styles.learn_more} to="/underconstruction">
            Learn More
          </Link>
        </div>
        <div className={styles.pricing_list}>
          <PricingItem
            item={{
              image: ProfileLinkImg,
              text: "Using Profile Link",
              buttonText: (
                <>
                  Get <strong>Pro</strong> for 24.99
                </>
              ),
            }}
          />
          <PricingItem
            item={{
              image: VideoEmbeddedImg,
              text: "Actual Video Embedded",
              buttonText: (
                <>
                  Get <strong>Premium</strong> for 59.99
                </>
              ),
            }}
          />
        </div>
      </div>
    </div>
  );
});

SharedContent.propTypes = {
  closeFn: PropTypes.func.isRequired,
};

export default SharedContent;
