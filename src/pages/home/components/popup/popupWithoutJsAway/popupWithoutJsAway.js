import React from "react";
import PropTypes from "prop-types";
import styles from "./popupWithoutJsAway.module.scss";
import SharedContent from "../sharedContent";

const PopupWithoutJsAway = ({ isOpen, closeFn }) => {
  return isOpen ? (
    <div className={styles.container}>
      {/* click away area */}
      <div className={styles.clickAwayArea} onClick={closeFn} />
      {/* popup content */}
      <SharedContent closeFn={closeFn} />
    </div>
  ) : (
    <></>
  );
};

PopupWithoutJsAway.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  closeFn: PropTypes.func.isRequired,
};

export default PopupWithoutJsAway;
