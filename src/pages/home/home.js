import React, { Component } from "react";
import styles from "./home.module.scss";
import Button from "./components/button/button";
import SharedContent from "./components/popup/sharedContent";
import PopupWithoutJsAway from "./components/popup/popupWithoutJsAway";
import PopupWithJsAway from "./components/popup/popupWithJsAway";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openPopup: false,
      openJSPopup: false,
    };
  }

  togglePopup = () => {
    this.setState({ openPopup: !this.state.openPopup });
  };

  toggleJSPopup = () => {
    this.setState({ openJSPopup: !this.state.openJSPopup });
  };

  render() {
    const { openPopup, openJSPopup } = this.state;

    return (
      <div className={styles.container}>
        <table>
          <tbody>
            <tr>
              <td>
                <Button onClick={this.togglePopup}>Without JS</Button>
                <br />
              </td>
              <td>
                closing pop up without javscript by knowing that the rendering
                in crp is sequential
              </td>
            </tr>
            <tr>
              <td>
                <Button onClick={this.toggleJSPopup}>With JS</Button>
              </td>
            </tr>
          </tbody>
        </table>

        <PopupWithoutJsAway closeFn={this.togglePopup} isOpen={openPopup} />
        <PopupWithJsAway closeFn={this.toggleJSPopup} isOpen={openJSPopup} />
      </div>
    );
  }
}

export default Home;
